from django.db import models

# create a blog models
# title
# pubdate 
# body
# image
class Blog(models.Model):
    title = models.CharField(max_length=255)
    pub_date = models.DateTimeField()
    body = models.TextField()
    image = models.ImageField(upload_to='images/')
    def summary(self):
    	return self.body[:100]
    def pub_date_pretty(self):
        return self.pub_date.strftime('%b %e %Y') 	
    def __str__(self):
        return self.title

# add blog to settings

# create a migration

# Migrate

#  Add to the admin

